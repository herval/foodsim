package FoodSim.Images;

import javax.swing.*;
import java.awt.*;
import java.awt.image.*;
import java.io.*;


/**
  * @author: Herval Freire de A. J�nior
  */
public class ThinConsumerLibrary extends ImageLibrary {

  public ThinConsumerLibrary(JPanel v){
    super(v);
    
    String imgs[] = { "images/magro1.gif", "images/magro2.gif", 
                      "images/magro3.gif", "images/magro4.gif",
                      "images/magro5.gif", "images/rostomagro.gif" };
    
    loadImages(imgs);
  }

}