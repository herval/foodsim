package FoodSim.Images;

import javax.swing.*;
import java.awt.*;
import java.awt.image.*;
import java.io.*;

/**
  * @author: Herval Freire de A. J�nior
  */
public class MetreLibrary extends ImageLibrary {

  public MetreLibrary(JPanel v){
    super(v);
    
    String imgs[] = { "images/metre1.gif", "images/metre2.gif", 
                      "images/metre3.gif", "images/metre4.gif",
                      "images/metre5.gif", "images/rostometre.gif" };
    
    loadImages(imgs);
  }

}
