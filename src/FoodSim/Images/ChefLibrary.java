package FoodSim.Images;

import javax.swing.*;
import java.awt.*;
import java.awt.image.*;
import java.io.*;

/**
 * @author: Herval Freire de A. J�nior
 */
public class ChefLibrary extends ImageLibrary {

  public ChefLibrary(JPanel v){
    super(v);
    
    String imgs[] = { "images/chef1.gif", "images/chef2.gif", 
                      "images/chef3.gif", "images/chef4.gif",
                      "images/chef5.gif", "images/rostochef.gif" };
    
    loadImages(imgs);
  }

}
