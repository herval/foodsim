package FoodSim.Images;

import javax.swing.*;
import java.awt.*;
import java.awt.image.*;
import java.io.*;

/**
  * Classe de encapsulamento de uma cole�ao de imagens
  * Um imageLibrary pode ser instanciado para cada tipo de sprite, como um FAT_BASTARD
  * ou um WAITER
  *
  * @author: Herval Freire de A. J�nior
  *
 */

public class ImageLibrary {

  public MediaTracker tracker; // carregador de imagens
  public JPanel view; // visao onde as imagens devem ser carregadas

  public BufferedImage[] images; // imagens desta library


  public ImageLibrary(JPanel view) {
    this.view = view;
  }

  // seta as imagens desta library
  public void loadImages(String url[]){
    //System.out.println(url.length+" quadros");
    tracker = new MediaTracker(view);

    images = new BufferedImage[url.length];

    for(int i = 0; i < url.length; i++){
      Image im = Toolkit.getDefaultToolkit().getImage(url[i]);
      tracker.addImage(im, i);

      try{
        tracker.waitForID(i);
        images[i] = new BufferedImage(im.getWidth(view),im.getHeight(view), BufferedImage.TYPE_INT_ARGB);
        images[i].createGraphics().drawImage(im, 0, 0, null);

      }catch(InterruptedException e){
        e.printStackTrace();
      }

    }
//    System.out.println("LOADED IMAGES");
  }
}
