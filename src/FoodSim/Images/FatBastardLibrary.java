package FoodSim.Images;

import javax.swing.*;
import java.awt.*;
import java.awt.image.*;
import java.io.*;

/**
  * @author: Herval Freire de A. J�nior
  */
public class FatBastardLibrary extends ImageLibrary {

  public FatBastardLibrary(JPanel v){
    super(v);
    
    String imgs[] = { "images/gordo1.gif", "images/gordo2.gif", 
                      "images/gordo3.gif", "images/gordo4.gif",
                      "images/gordo5.gif", "images/rostogordo.gif" };
    
    loadImages(imgs);
  }

}
