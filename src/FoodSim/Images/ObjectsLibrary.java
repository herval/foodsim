package FoodSim.Images;

import javax.swing.*;
import java.awt.*;
import java.awt.image.*;
import java.io.*;

/**
  * @author: Herval Freire de A. J�nior
  */
public class ObjectsLibrary extends ImageLibrary {

  public ObjectsLibrary(JPanel v){
    super(v);
    
    String imgs[] = { "images/cenario.gif", "images/hamburguer.gif", 
                      "images/mesa.gif" };
    
    loadImages(imgs);
  }

}
