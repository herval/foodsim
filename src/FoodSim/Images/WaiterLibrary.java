package FoodSim.Images;

import javax.swing.*;
import java.awt.*;
import java.awt.image.*;
import java.io.*;

/**
  * @author: Herval Freire de A. J�nior
  */
public class WaiterLibrary extends ImageLibrary {

  public WaiterLibrary(JPanel v){
    super(v);
    
    String imgs[] = { "images/servente1.gif", "images/servente2.gif", 
                      "images/servente3.gif", "images/servente4.gif",
                      "images/servente5.gif", "images/rostoservente.gif" };
    
    loadImages(imgs);
  }

}
