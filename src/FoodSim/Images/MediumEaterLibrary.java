package FoodSim.Images;

import javax.swing.*;
import java.awt.*;
import java.awt.image.*;
import java.io.*;


/**
  * @author: Herval Freire de A. J�nior
  */
public class MediumEaterLibrary extends ImageLibrary {

  public MediumEaterLibrary(JPanel v){
    super(v);
    
    String imgs[] = { "images/medio1.gif", "images/medio2.gif", 
                      "images/medio3.gif", "images/medio4.gif",
                      "images/medio5.gif", "images/rostomedio.gif" };
    
    loadImages(imgs);
  }

}


