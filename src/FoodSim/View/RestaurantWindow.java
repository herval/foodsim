package FoodSim.View;

import FoodSim.Food.*;
import FoodSim.Actors.*;
import FoodSim.*;
import FoodSim.Images.*;

import javax.swing.*;
import java.util.*;
import java.awt.*;
import java.awt.event.*;

/**
 * @author: Herval Freire de A. J�nior
 */
public class RestaurantWindow extends JFrame {
  VisualRestaurant rest;

  private JButton add1, add2;
  private JButton rem1, rem2;
  public BorderLayout borderLayout1 = new BorderLayout();
  public JPanel eatersPanel = new JPanel();
  public JPanel cookersPanel = new JPanel();
  public RestaurantGround groundPanel = new RestaurantGround(this);
  public JLabel status;


  public RestaurantWindow(){
     rest = new VisualRestaurant(this);
     
     try {
       jbInit();
       
       Thread rest = new Thread(groundPanel);
       rest.setPriority(Thread.MIN_PRIORITY);
       rest.start();
     }
     catch(Exception e) {
//       System.out.println(e.toString());
       e.printStackTrace();
     }
  }

  public VisualRestaurant getRestaurant(){
    return rest;
  }  

  private void jbInit() throws Exception {
    add1 = new JButton("Adicionar");
    add1.setPreferredSize(new Dimension(90, 30));
    add1.addActionListener(new ActionListener() {
         public void actionPerformed(ActionEvent e){
           rest.addConsumer(new VisualEater(rest, (int)(((Math.random())*10)%3), "Ze-"+rest.getEaters().size()));
         }
      }
    );

    rem1 = new JButton("Remover");
    rem1.setPreferredSize(new Dimension(120, 30));
    rem1.addActionListener(new ActionListener() {
         public void actionPerformed(ActionEvent e){
           if(rest.getEaters().size() > 0){
              VisualEater eat = (VisualEater)rest.getEaters().remove(rest.getEaters().size()-1);
              
              Component[] comps = eatersPanel.getComponents();
              for(int i = 0; i < comps.length; i++){
                if(comps[i] instanceof EaterButton) {
                   if( ((EaterButton)comps[i]).getEater() == eat) {
                     eatersPanel.remove(comps[i]);
                     ((EaterButton)comps[i]).getEater().stopEating();
                     break;
                   }
                }  // if
              } // for
           }
         }
      }
    );

    add2 = new JButton("Adicionar");
    add2.setPreferredSize(new Dimension(120, 30));
    add2.addActionListener(new ActionListener() {
       public void actionPerformed(ActionEvent e){
         rest.addCooker(new VisualCooker(rest, (int)(((Math.random())*10)%3), "Pierre-"+rest.getCookers().size()));
       }
      } 
    );

    rem2 = new JButton("Remover");
    rem2.setPreferredSize(new Dimension(90, 30));
    rem2.addActionListener(new ActionListener() {
         public void actionPerformed(ActionEvent e){
            if(rest.getCookers().size() > 0){
               VisualCooker cook = (VisualCooker)rest.getCookers().remove(rest.getCookers().size()-1);
         
               Component[] comps = cookersPanel.getComponents();
               for(int i = 0; i < comps.length; i++){
                 if(comps[i] instanceof CookerButton) {
                    if( ((CookerButton)comps[i]).getCooker() == cook) {
                      cookersPanel.remove(comps[i]);
                      ((CookerButton)comps[i]).getCooker().stopCooking();
                      break;
                    }
                 }  // if
               } // for
            }
         }   
      }
    );
  
    setSize(new Dimension(640, 480));
    getContentPane().setLayout(borderLayout1);
    cookersPanel.setBackground(Color.white);
    cookersPanel.setPreferredSize(new Dimension(160, 480));
    eatersPanel.setBackground(Color.white);
    eatersPanel.setPreferredSize(new Dimension(160, 480));
    groundPanel.setBackground(Color.gray);
    groundPanel.setPreferredSize(new Dimension(500, 500));
    
    
    status = new JLabel();
    updateSandwichCount();

    getContentPane().add(eatersPanel,  BorderLayout.WEST);
    getContentPane().add(cookersPanel, BorderLayout.EAST);
    getContentPane().add(groundPanel, BorderLayout.CENTER);
    getContentPane().add(status, BorderLayout.SOUTH);
        
    eatersPanel.add(add1);
    eatersPanel.add(rem1);

    cookersPanel.add(add2);
    cookersPanel.add(rem2);
    
    pack();
    show();
  }

  public void updateSandwichCount(){
    status.setText(getRestaurant().getFoodAvailable()+" sandu�ches na mesa");
  }

  public static void main(String args[]){
    RestaurantWindow cefetCantina = new RestaurantWindow();
    
    cefetCantina.addWindowListener( new WindowAdapter(){
        public void windowClosing(WindowEvent e){
          System.exit(0);
        }
      }
    );

  }
}



class RestaurantGround extends JPanel implements Runnable {
  RestaurantWindow rest;
  ImageLibrary im;
  
  public RestaurantGround(RestaurantWindow m){
    rest = m;
    
    im =  new ObjectsLibrary(this);

  }

  public void paint(Graphics g){
  
    g.drawImage(im.images[Objects.ROOM], 0, 0, this);  
    
    // DESENHO DOS COZINHEIROS
    int offsetX = 0;
    int offsetY = 0;
        
    Vector cookers = rest.getRestaurant().getCookers();
    for(int i = 0; i < cookers.size(); i++){
       VisualCooker gr = (VisualCooker)cookers.get(i);
       
       if(i > 5) {
         offsetX = gr.getImage().getHeight();
         offsetY = -(this.getHeight()/3-(10*4)+((gr.getImage().getHeight())*4));
       }
       if(i > 11) {
         offsetX = (gr.getImage().getHeight()+2)*2;
//         offsetY = -(this.getHeight()/3-(10*10)-15+((gr.getImage().getHeight())*10));
       }
       
       g.drawImage(gr.getImage(), this.getWidth()/2+gr.getImage().getWidth()-10+offsetX,
            this.getHeight()/3-(10*i)-15+((gr.getImage().getHeight())*i)+offsetY, this);
    }

    // DESENHO DOS CONSUMIDORES    
    offsetX = offsetY = 0;

    Vector eaters = rest.getRestaurant().getEaters();
    for(int i = 0; i < eaters.size(); i++){
       VisualEater gr = (VisualEater)eaters.get(i);

       if(i > 5) {
         offsetX = -gr.getImage().getHeight();
         offsetY = -this.getHeight()/3+((gr.getImage().getHeight()/2))-15;
       }
       if(i > 11) {
         offsetX = -(gr.getImage().getHeight()+2)*2;
         offsetY = -this.getHeight()/3+((gr.getImage().getHeight()/2))-15;
       }

       g.drawImage(gr.getImage(), this.getWidth()/3-gr.getImage().getWidth()/2-(3*i)+10+offsetX,
            this.getHeight()/3+((gr.getImage().getHeight()/2)*i)-15+offsetY, this);
    }

    // DESENHO DO BUFFER... ERR... MESA
    g.drawImage(im.images[Objects.TABLE], this.getWidth()/3+20, this.getHeight()/3, this);


    // ATUALIZACAO DOS BOTOES
    Component[] comp1 = rest.eatersPanel.getComponents();
    for(int i = 0; i < comp1.length; i++){
      if(comp1[i] instanceof EaterButton){
        ((EaterButton)comp1[i]).updateName();
      }
    }

    Component[] comp2 = rest.cookersPanel.getComponents();
    for(int i = 0; i < comp2.length; i++){
      if(comp2[i] instanceof CookerButton){
        ((CookerButton)comp2[i]).updateName();
      }
    }


    // DESENHO DA COMIDA    
    for(int i = 0; i < rest.getRestaurant().getFoodAvailable(); i++) {
     while(i<10 && i < rest.getRestaurant().getFoodAvailable()) {
      g.drawImage(im.images[Objects.FOOD], this.getWidth()/2-10, (this.getHeight()/3)+(12*i)+10, this);
      i++;
     }
     int j = 0;  
     while(i<21 && i < rest.getRestaurant().getFoodAvailable()) {
      g.drawImage(im.images[Objects.FOOD], this.getWidth()/2+im.images[Objects.FOOD].getWidth()-5,
                  (this.getHeight()/3)+(12*j)+10, this);
      i++;
      j++;
     }

     j = 0;
     while(i < rest.getRestaurant().getFoodAvailable()) {
      g.drawImage(im.images[Objects.FOOD], this.getWidth()/2+2*im.images[Objects.FOOD].getWidth()-5,
                  (this.getHeight()/3)+(12*j)+10, this);
      i++;
      j++;
     }
    }

    g.drawString(rest.getRestaurant().getFoodAvailable()+" sandwichs on the table", 10, 15);
    g.drawString("Copyright 2002 � Herval Freire - All rights unreserved and fucked up", 10, this.getWidth()-5);
  }  


  public void run(){ 
    long delay = 100;
    long actTime;

    while(true){
     actTime = System.currentTimeMillis();
     while(System.currentTimeMillis()-actTime < delay);
     rest.repaint();
    }
  }
}


class EaterButton extends JToggleButton {

  private VisualEater me;
  private String name = "";
  private String legenda = "";
  private RestaurantWindow where; // restauranteWindow que registra este botao

  public VisualEater getEater(){
    return me;
  }


  public void setLegenda(String leg){
    this.legenda = leg;
  }

  public void updateName(){
    legenda = me.getStateName();
    this.setText(name+" ("+legenda+")");
  }  

  public EaterButton(VisualEater c, RestaurantWindow tela){
    me = c;

    this.where = tela;
    this.name = c.getName();
    this.legenda = c.getStateName();    
    this.setText(name+" ("+legenda+")");

    this.setIcon(new ImageIcon(c.getFace()));
    
    this.addActionListener( new ActionListener() {
        public void actionPerformed(ActionEvent e){
          if(!EaterButton.this.isSelected()){
            me.stopEating();
          } else {
            me.startEating();
          }
        }
      }
    );
    
    this.addMouseListener( new MouseAdapter() {
        public void mouseEntered(MouseEvent e){
          where.status.setText(me.getName()+": J� comi "+me.getHamburguerCount()+" pratos deliciosos!");
        }
    
        public void mouseExited(MouseEvent e){
          where.updateSandwichCount();
        }
      }
    );
    
    
    
  }
}


class CookerButton extends JToggleButton {

  private VisualCooker me;
  private String name = "";
  private String legenda = "";
  private RestaurantWindow where; // restauranteWindow que registra este botao
    
  public VisualCooker getCooker(){
    return me;
  }
 
  public void setLegenda(String leg){
    this.legenda = leg;
  }

  public void updateName(){
    legenda = me.getStateName();
    this.setText(name+" ("+legenda+")");
  }  

  public CookerButton(VisualCooker c, RestaurantWindow tela){
    me = c;
    
    this.where = tela;
    this.setIcon(new ImageIcon(c.getFace()));
    this.name = c.getName();
    this.legenda = c.getStateName();

    this.setText(name+" ("+legenda+" )");
        
    this.addActionListener( new ActionListener() {
        public void actionPerformed(ActionEvent e){
          if(!CookerButton.this.isSelected()){
            me.stopCooking();
          } else {
            me.startCooking();
          }
        }
      }
    );

    this.addMouseListener( new MouseAdapter() {
        public void mouseEntered(MouseEvent e){
          where.status.setText(me.getName()+": J� preparei "+me.getHamburguerCount()+" refei��es nutritivas!");
        }
    
        public void mouseExited(MouseEvent e){
          where.updateSandwichCount();
        }
      }
    );

    
  }
}