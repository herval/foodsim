package FoodSim.View;

import FoodSim.Food.*;
import FoodSim.Restaurant;
import FoodSim.Actors.*;
import FoodSim.Images.*;

import java.awt.image.*;
/**
  * Classe VisualCooker
  * 
  * Classe de representa��o visual de um cozinheiro
  * 
  *
  * @author: Herval Freire de A. J�nior
  *
  */
public class VisualCooker extends Cooker implements VisualActor {

  ImageLibrary lib;
  public int x = 0;
  public int y = 0; // posicao atual do cozinheiro
  int imgAtual = 0;
 
  /** 
    * Cria um cozinheiro trabalhando no restaurante dado
    */
  public VisualCooker(VisualRestaurant work, int knowHow, String name){
    super(work, knowHow, name);
    
    switch(knowHow) {
       case METRE: lib = new MetreLibrary(work.getDisplay().groundPanel); break;
       case CHEF: lib = new ChefLibrary(work.getDisplay().groundPanel); break;
       default: lib = new WaiterLibrary(work.getDisplay().groundPanel); break;
    }
  }

  public BufferedImage getFace(){
    return lib.images[lib.images.length-1]; // �ltima imagem � o rosto
  }
 
  
  public BufferedImage getImage(){
    imgAtual = state;
    return lib.images[imgAtual];
  }
  

}