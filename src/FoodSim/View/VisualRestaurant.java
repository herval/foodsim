package FoodSim.View;

import FoodSim.Food.*;
import FoodSim.Actors.*;
import FoodSim.*;

import javax.swing.*;
import java.util.*;
import java.awt.*;
import java.awt.event.*;

/**
  * Classe VisualRestaurant
  *
  * Uma representa��o visual (janela) de um objeto restaurante
  * Extende restaurante e controla um JFrame com v�rios bot�es (um para cada VisualEater
  * ou VisualCooker)
  *
  * @author: Herval Freire de A. J�nior
  *
  */
public class VisualRestaurant extends Restaurant {

  RestaurantWindow janela;

  public VisualRestaurant(RestaurantWindow jan){
    janela = jan;

  }


  /** Pede por comida e espeda indefinidamente */
  public Food getFood(VisualEater e){
    Food ret = super.getFood(e);
    janela.updateSandwichCount();

    return ret;
  }

  /** Adiciona um sandu�che ao balc�o */
  public void addFood(Food f, Cooker c){
    super.addFood(f, c);
    janela.updateSandwichCount();

  }

  public void addCooker(VisualCooker c){
    super.addCooker(c);
    
    CookerButton cooker1 = new CookerButton(c, janela);
    cooker1.setSize(new Dimension(40,30));
    janela.cookersPanel.add(cooker1);
    janela.pack();
    janela.cookersPanel.repaint();

  }

  public void addConsumer(VisualEater c){
    super.addConsumer(c);

    EaterButton cons1 = new EaterButton(c, janela);
    cons1.setSize(new Dimension(40,30));    
    janela.eatersPanel.add(cons1);
    janela.pack();
    janela.eatersPanel.repaint();
    
  }

  public RestaurantWindow getDisplay(){
    return janela;
  }  

}

