package FoodSim.View;

import FoodSim.Actors.*;
import FoodSim.Food.*;
import FoodSim.Restaurant;
import FoodSim.Images.*;

import java.awt.image.*;

/**
  * Classe VisualEater
  * 
  * Um Eater com uma imagem associada
  * Esta classe manipula um ImageLibrary especifico
  *
  * @author: Herval Freire de A. J�nior
  *
  */
public class VisualEater extends Eater implements VisualActor {

  ImageLibrary lib;
  public int x = 0;
  public int y = 0; // posicao atual do cozinheiro
  int imgAtual = 0;

  /** 
    * Cria um consumidor no restaurante dado
    */
  public VisualEater(VisualRestaurant rest, int hung, String name){
    super(rest, hung, name);
    
    switch(hung) {
       case THIN_CONSUMER: lib = new ThinConsumerLibrary(rest.getDisplay().groundPanel); break;
       case FAT_BASTARD: lib = new FatBastardLibrary(rest.getDisplay().groundPanel); break;
       default: lib = new MediumEaterLibrary(rest.getDisplay().groundPanel); break;
    }
    
    
  }
  
  public BufferedImage getFace(){
    return lib.images[lib.images.length-1]; // �ltima imagem � o rosto
  }
  
  public BufferedImage getImage(){
    imgAtual = state;
    return lib.images[imgAtual];
  }

}