package FoodSim.Actors;

/**
 * @author: Herval Freire de A. J�nior
 */
public interface Actor {

  /** M�todo que "faz alguma coisa" */
  public void doTheRun() throws Exception;

}
