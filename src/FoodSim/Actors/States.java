package FoodSim.Actors;


/**
  * @author: Herval Freire de A. J�nior
  */
public interface States {

  // estados comuns
  
  public int RESTING = 3;
  public int WAITING = 0;

  // estados de cozinheiro
  public int COOKING = 1;
  public int SERVING = 2;
  
  // estados de cliente
  public int TAKING = 1;
  public int EATING = 2;
  
  public String getStateName();

}