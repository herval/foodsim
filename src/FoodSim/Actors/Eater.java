package FoodSim.Actors;

import FoodSim.Food.*;
import FoodSim.Restaurant;

/**
  * Classe Eater
  * 
  * Um thread que representa um consumidor
  * Cada consumidor possui seu n�vel de fome, que indica quantos sandu�ches o mesmo come
  * antes de se encher (por um tempo)
  * Cada consumidor � associado tamb�m a um restaurante, onde o mesmo se alimenta
  *
  * @author: Herval Freire de A. J�nior
  */
public class Eater extends Thread implements Hunger, States, Actor {
  public int state = RESTING;

  protected int hunger; /** N�vel de habilidade deste cozinheiro */
  protected Restaurant rest; /** Restaurante onde trabalha o cozinheiro */
  
  protected boolean keepEating = false;
  
  protected int eaten = 0; /** Quantidade j� cozinhada */
  
  /** 
    * Cria um consumidor no restaurante dado
    */
  public Eater(Restaurant rest, int hung, String name){
    super(name);
    super.setPriority(Thread.MIN_PRIORITY);
    this.hunger = hung;
    this.rest = rest;
  }
  
   public String getStateName(){
    switch(state) {
      case RESTING: return "Roncando";
      case WAITING: return "Esperando";
      case TAKING: return "Exigindo comida";
      case EATING: return "Mastigando";
      default: return "Zanzando";
    }
  }    
  
  public int getHunger(){
    return hunger;
  }

  public void setKeepEating(boolean is){
    keepEating = is;
  }

  public void startEating(){
    keepEating = true;
    if(!isAlive()) start();
  }  

  public int getHamburguerCount(){
    return eaten;
  }
  
  public void stopEating(){
    keepEating = false;
  }  

  public void doTheRun() throws Exception{

     state = TAKING;
     this.sleep(Hunger.WaitingTime - this.getHunger()*500);
     rest.getFood(this);
     eaten++;

     state = EATING;
     this.sleep(Hunger.EatingTime - this.getHunger()*500);

     state = WAITING;
     this.sleep(Hunger.RestingTime - this.getHunger()*500);
     this.yield();

  }

  public void run(){
      try{
        while(true) {
          while(keepEating){
 //           System.out.println("EATIN");
//            state = WAITING;
            doTheRun();
          }
//          System.out.println("NOT EATIN"); 
          state = RESTING;
        } 
      }catch(Exception e){
        e.printStackTrace();
        stopEating();
        // MATAR O CLIENTE AQUI!!
      }
  }  


}