package FoodSim.Actors;

/**
  * Interface Hunger: uma defini��o de qu�o faminto � um indiv�duo
  *
  * @author: Herval Freire de A. J�nior
  *
  */
public interface Hunger {

  public final int EatingTime = 3000; /** Menor tempo que um sandu�che demora para ser mastigado */
  public final int WaitingTime = 3000; /** Menor tempo que um cliente espera */
  public final int RestingTime = 3000; /** Menor tempo que um cliente descansa */


  public final int THIN_CONSUMER = 0;
  public final int MEDIUM_EATER = 1;
  public final int FAT_BASTARD = 2;
  
  public int getHunger();

}