package FoodSim.Actors;

/**
  * Interface KnowHow: uma defini��o de qu�o bom � um cozinheiro
  * @author: Herval Freire de A. J�nior
  *
  */
public interface KnowHow {

  public final int CookingTime = 3000; /** Menor tempo que um sandu�che demora para cozinhar */
  public final int ServingTime = 3000; /** Menor tempo que um sandu�che demora para ser servido */
  public final int RestingTime = 3000; /** Menor tempo que um cozinheiro espera para cozinhar novamente */


  public final int WAITER = 0;
  public final int METRE = 1;
  public final int CHEF = 2;
  
  public int getKnowHow();

}