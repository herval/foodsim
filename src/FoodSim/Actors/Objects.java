package FoodSim.Actors;


/**
  * @author: Herval Freire de A. J�nior
  */
public interface Objects {
  
  public int ROOM = 0;
  public int FOOD = 1;
  public int TABLE = 2;

}