package FoodSim.Actors;

import FoodSim.Food.*;
import FoodSim.Restaurant;

/**
  * Classe Cooker
  * 
  * Um thread que representa um cozinheiro
  * Cada cozinheiro possui seu n�vel de habilidade em cozinha, que determina o n�mero 
  * de sandu�ches que o mesmo cozinha num intervalo de tempo.
  * Cada cozinheiro possui ainda um restaurante relacionado, onde o mesmo trabalha
  * 
  * @author: Herval Freire de A. J�nior
  * 
  */
public class Cooker extends Thread implements KnowHow, States, Actor {
  public int state = RESTING; // estado atual do cozinheiro

  protected int knowHow; /** N�vel de habilidade deste cozinheiro */
  protected Restaurant work; /** Restaurante onde trabalha o cozinheiro */
  
  protected boolean keepCooking = false;
  
  protected int cooked = 0; /** Quantidade j� cozinhada */
  
  /** 
    * Cria um cozinheiro trabalhando no restaurante dado
    */
  public Cooker(Restaurant work, int knowHow, String name){
    super(name);
    super.setPriority(Thread.MIN_PRIORITY);
    this.knowHow = knowHow;
    this.work = work;
  }
  
  public int getKnowHow(){
    return knowHow;
  }

  public String getStateName(){
    switch(state) {
      case RESTING: return "Descansando";
      case WAITING: return "Esperando";
      case COOKING: return "Cozinhando";
      case SERVING: return "Servindo";
      default: return "Zanzando";
    }
  }  

  public void setKeepCooking(boolean is){
    keepCooking = is;
  }

  public void startCooking(){
    if(!isAlive()) start();
    keepCooking = true;
  }  

  public void stopCooking(){
    keepCooking = false;
  }  


  public int getHamburguerCount(){
    return cooked;
  }


  public void doTheRun() throws Exception {
     state = COOKING;
     this.sleep(KnowHow.CookingTime - this.getKnowHow()*500);

     state = SERVING;
     this.sleep(KnowHow.ServingTime - this.getKnowHow()*500);

     work.addFood(new Food(), this);
     cooked++;

     state = WAITING;
     this.sleep(KnowHow.RestingTime - this.getKnowHow()*500);
     this.yield();
  }

  public void run(){
      try{
        while(true) {
         while(keepCooking){
           doTheRun();
         }
         state = RESTING;
        }   
      }catch(Exception e){
        e.printStackTrace();
        // MATAR O GAR�OM AQUI!!
        this.stopCooking();
      }
  }  


}