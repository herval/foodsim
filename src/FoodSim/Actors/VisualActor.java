package FoodSim.Actors;

import java.awt.image.*;

/**
  * @author: Herval Freire de A. J�nior
  */
public interface VisualActor {

  public BufferedImage getImage();
  public BufferedImage getFace();

}