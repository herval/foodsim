package FoodSim;

import FoodSim.Food.*;
import FoodSim.Actors.*;

import java.util.*;

/**
  * Classe Restaurant
  * 
  * Um "pool" virtual de threads consumidores e produtores
  *
  * @author: Herval Freire de A. J�nior
  *
  */
public class Restaurant implements FoodRepository {

  private static boolean textual = false;

  protected Vector clients; /** Vetor de threads relativo aos clientes */
  protected Vector cookers; /** Vetor de threads relativo aos cozinheiros */
  
  protected Stack foodPile; /** "pilha" de comida dispon�vel no balc�o */
  
  public Restaurant(){
    clients = new Vector();
    cookers = new Vector();

    foodPile = new Stack();    
  }


  public Vector getCookers(){
    return cookers;
  }  

  public Vector getEaters(){
    return clients;
  }  
  
  /** Pede por comida e espeda indefinidamente */
  public synchronized Food getFood(Eater e){
    boolean waited = false;
//    long init = System.currentTimeMillis();
//    long tFinal = 0;
    
    try{
      while(foodPile.size() <= 0) wait();
    }catch(Exception er){
      er.printStackTrace();
    }
    
  /*  while(foodPile.size() <= 0) tFinal = System.currentTimeMillis();
    
    if(tFinal > 0){
      if(textual) System.out.println("Esperei "+(tFinal-init)+" milissegundos para comer!");
    }*/

    if(textual) System.out.println(e.getName()+" comeu");    
    if(textual) System.out.println(getFoodAvailable()+" sanduiches sobrando");    
 
    Food retorno = (Food)foodPile.pop();

    notifyAll();
    
    return retorno;
  }

  /** Adiciona um sandu�che ao balc�o */
  public synchronized void addFood(Food f, Cooker c){

//    long init = System.currentTimeMillis();
//    long tFinal = 0;

    try{
      while(foodPile.size() >= MAXFOOD) wait();
    }catch(Exception e){
      e.printStackTrace();
    }
    
/*    while(foodPile.size() >= MAXFOOD) {
      tFinal = System.currentTimeMillis();
    }
*/

    if(textual) System.out.println(c.getName()+" cozinhou");
    foodPile.push(f);
    
    notifyAll();
  }  

  /** Verifica quantos sandu�ches est�o prontos */
  public int getFoodAvailable(){
    if(textual) System.out.println("Comida: "+foodPile.size());
    return foodPile.size();
  }

  public void addCooker(Cooker c){
    cookers.add(c);
  }
 
  public void addConsumer(Eater c){
    clients.add(c);
  }
  
  
  public static void main(String args[]){
    Restaurant cefetCantina = new Restaurant();

    Cooker pierre = new Cooker(cefetCantina, KnowHow.CHEF, "Pierre");
    Cooker joao = new Cooker(cefetCantina, KnowHow.CHEF, "Joao");
    Cooker ze = new Cooker(cefetCantina, KnowHow.CHEF, "Ze");

    Eater maryFat = new Eater(cefetCantina, Hunger.FAT_BASTARD, "Maria");
    Eater boy = new Eater(cefetCantina, Hunger.THIN_CONSUMER, "Joaozinho");

    cefetCantina.addCooker(pierre);
    cefetCantina.addCooker(joao);
    cefetCantina.addCooker(ze);

    cefetCantina.addConsumer(boy);
    cefetCantina.addConsumer(maryFat);

//    maryFat.startEating();
    boy.startEating();
    
    joao.startCooking();
    pierre.startCooking();
    ze.startCooking();
  }

}