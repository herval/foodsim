package FoodSim;

import FoodSim.Food.*;
import FoodSim.Actors.*;

/**
  * @author: Herval Freire de A. J�nior
  */
public interface FoodRepository {

  public int MAXFOOD = 20;
  
  public Food getFood(Eater e);
  
  public void addFood(Food f, Cooker c);
  
  public int getFoodAvailable();

}