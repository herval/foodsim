FoodSim v0.003 (micro-alpha-edition 3)

Herval Freire de Albuquerque J�nior
herval@ieg.com.br


Programa simples para demonstrar a funcionalidade dos Threads em Java

O sistema modela um ambiente Produtor-Consumidor com um �nico Buffer.

A classe Restaurant serve como um tipo de "monitor", mandando os Threads aguardarem e prosseguirem

Requisitos: Jdk1.3 ou superior, muita mem�ria RAM (recomendado) :)